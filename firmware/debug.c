#include <avr/io.h>
#include <util/delay.h>
#include <avr/wdt.h>
#include <math.h>

#include "lib/prgKeyboard.h"

#define BLOCK_WIDTH 256

int debug_menu(void) {
    LED_PORT |= (1 << LED_RED);
    _delay_ms(250);
    LED_PORT |= (1 << LED_GREEN);
    LED_PORT &= ~(1 << LED_RED);
    _delay_ms(250);
    LED_PORT &= ~(1 << LED_GREEN);
    LED_PORT |= (1 << LED_RED);
    _delay_ms(250);
    LED_PORT |= (1 << LED_GREEN);
    LED_PORT &= ~(1 << LED_RED);
    _delay_ms(250);
    LED_PORT &= ~(1 << LED_GREEN);

    while (1) {
        if (getKey()) {
            int y;
            y = getKey();
            #define VALUE 42

            if (y == 1) { // eeprom test
                eeprom_test();
            }
            else if (y == 2) { // eeprom test
                eeprom_erase();
            }
            else if (y == Key_MODE) {
                return 0;
            }
            else {
                PORTD |= (1 << PD4);
                _delay_ms(200);
                PORTD &= ~(1 << PD4);
                _delay_ms(200);
                PORTD |= (1 << PD4);
                _delay_ms(200);
                PORTD &= ~(1 << PD4);
            }
        }

        _delay_ms(1);
    }
}

void eeprom_test(void) {
    EEOpen(); //open the eeprom
    uint8_t block; // 4 blocks -> one key
    uint8_t isOK;
    isOK=1;
    for (block=0; block<(1024*16/BLOCK_WIDTH); block++) { // 16KByte/256 -> 64 loops
        uint16_t i;
        for (i=0; i < BLOCK_WIDTH; i++) {
            if (i%32 == 0) {
                LED_PORT ^= (1 << LED_RED); //flash the leds
                LED_PORT ^= (1 << LED_GREEN);
            }
            EEWriteByte(block*BLOCK_WIDTH+i,i); //write
            if (EEReadByte(block*BLOCK_WIDTH+i) != i) { //read & compare
                i=254;
                block=254;
                isOK = 0;
                break;
            }
        }
    }
    if (isOK) { //SHow result
        LED_PORT |= (1 << LED_GREEN);
        LED_PORT &= ~(1 << LED_RED);
    }
    else {
        LED_PORT &= ~(1 << LED_GREEN);
        LED_PORT |= (1 << LED_RED);
    }
    while(!getKey()); //wait for the user to confirm
    while(getKey()); // released
    LED_PORT &= ~(1 << LED_RED);
    LED_PORT &= ~(1 << LED_GREEN);
}

void eeprom_erase(void) {
    EEOpen(); //open the eeprom
    uint8_t block; // 4 blocks -> one key
    uint8_t isOK;
    for (block=0; block<(1024*16/BLOCK_WIDTH); block++) { // 16KByte/256 -> 64 loops
        uint16_t i;
        for (i=0; i < BLOCK_WIDTH; i++) {
            if (i%32 == 0) {
                LED_PORT ^= (1 << LED_RED); //flash the leds
                LED_PORT ^= (1 << LED_GREEN);
            }
            EEWriteByte(block*BLOCK_WIDTH+i,0xff); // erase->0xff
        }
    }
        LED_PORT |= (1 << LED_GREEN);
        LED_PORT &= ~(1 << LED_RED);

    while(!getKey()); //wait for the user to confirm
    while(getKey()); // released
    LED_PORT &= ~(1 << LED_RED);
    LED_PORT &= ~(1 << LED_GREEN);
}



