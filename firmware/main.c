// DATE/TIME: 2011/10/28 17:34

#ifndef F_CPU
#warning "F_CPU war noch nicht definiert, wird nun mit 16000000 definiert"
#define F_CPU 16000000UL     /* Quarz mit 16 Mhz  */
#endif

#include <avr/io.h>
#include <util/delay.h>
#include <avr/wdt.h>

#include "lib/prgKeyboard.h"
#include "debug.h"
#include "keyboard.h"

int main(void) {
    initPorts(); //Init all the stuff
    if (getKey() == Key_MODE) debug_menu();
    keyboard();
    return 0;
}
