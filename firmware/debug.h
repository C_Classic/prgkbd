#ifndef _DEBUG_H_
#define _DEBUG_H_

    int debug_menu(void);
    void eeprom_test(void);
    void eeprom_erase(void);

#endif
