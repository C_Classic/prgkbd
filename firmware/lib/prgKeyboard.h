#ifndef _PRGKEYBOARD_H_
#define _PRGKEYBOARD_H_
// DEFINES:
// General foo
    #define TRUE 1
    #define FALSE 0

// Keypad-defines
    #define Keypad_PORT_Y PORTB
    #define Keypad_DDR_Y DDRB
    #define Keypad_PORT_X PORTC
    #define Keypad_DDR_X DDRC
    #define Keypad_PIN_X PINC
    #define Keypad_MDPORT PORTD
    #define Keypad_MDDDR DDRD
    #define Keypad_MDPIN PIND
    #define Keypad_X1 PC0
    #define Keypad_X2 PC1
    #define Keypad_X3 PC2
    #define Keypad_X4 PC3
    #define Keypad_Y1 PB0
    #define Keypad_Y2 PB1
    #define Keypad_Y3 PB2
    #define Keypad_Y4 PB3
    #define Keypad_MD PD6
    #define KEYDELAY __builtin_avr_delay_cycles(1);__builtin_avr_delay_cycles(1); // May be increased if the keys don't work properly
    #define Key_MODE 255
// EEPROM (24lc64) defines:
    #define EEPROM_PORT PORTC
    #define EEPROM_DDR DDRC
    #define EEPROM_SDA PC4
    #define EEPROM_SCL PC5
	// #define EEPROM_WP PC6 --> cannot be used because of an error in the circuit design.
// LEDS
    #define LED_PORT PORTD
    #define LED_DDR DDRD
    #define LED_RED PD4
    #define LED_GREEN PD5


// FUNCTIONS:
// General hardware functions
    void initPorts(void);
    uint8_t getKey();

// 24lc64 functions
    void EEOpen();
    uint8_t EEWriteByte(uint16_t,uint8_t);
    uint8_t EEReadByte(uint16_t address);

#endif
