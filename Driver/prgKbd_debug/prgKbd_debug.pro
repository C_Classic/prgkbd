#-------------------------------------------------
#
# Project created by QtCreator 2011-12-26T18:56:25
#
#-------------------------------------------------

QT       += core

QT       -= gui

TARGET = prgKbd_debug
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app


SOURCES += main.cpp

LIBS += -lusb
