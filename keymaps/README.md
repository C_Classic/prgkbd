# Programmierbare Tastatur mit V-USB -- Keymaps#
*Copyright (C) 2013 C_Classic <CClassicVideos@aol.com>*

Fertige Tastaturbelegungen für das Programmable Keyboard.

## Installation ##
1. PC-Software herunterladen, kompilieren und wenn gewünscht nach `/usr/local/bin/` kopieren.
2. Tastaturbelegung herunterladen
3. Tastaturbelegung gegebenenfalls per `bunzip2 file.bz2` dekomprimieren.
4. PC-Software als root starten
5. Zum Übertragen der Belegung `backup file` eingeben. Dieser Vorgang dauert circa 4 Minuten.

Anmerkung: Wenn eine Tastaturbelegung von der Tastatur gesichert werden soll, kann man `dump file` verwenden.

## Download ##
### Programme ###
* **CNC-Software Mach3**: [Belegungsplan](../../raw/master/keymaps/mach3.pdf) - [Download](../../raw/master/keymaps/mach3.bz2)

### Spiele ###
*Keine Belegungen verfügbar*

## Fehlersuche ##
* Es werden folgende Bibliotheken zusätzlich zu den Standardbibliotheken benötigt: `libqt4-core libusb++-0.1-4c2`
* Zum Kompilieren müssen auch die entsprechenden -dev-Pakete installiert sein.
* Es muss `qmake` ausgeführt werden, um ein normales Makefile zu generieren.
