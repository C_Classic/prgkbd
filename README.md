# Programmierbare Tastatur mit V-USB #
*Copyright (C) 2013 C_Classic <CClassicVideos@aol.com>*

Soft- und Firmware zur programmierbaren Tastatur von CClassicVideos.

## Weitere Informationen ##
Informationen zur Hardware können auf der [Projektseite](http://cclassic.users.sourceforge.net/wp/?page_id=32) gefunden werden.
